import React, {useState} from "react";
import ColorLensIcon from '@mui/icons-material/ColorLens';
import {SketchPicker} from "react-color";
import '../../styles/style.css';
// import {BlockPicker} from "react-color";


export default function Note(props) {

  const characterLimit="200";

  const[colorChange, setColorChange] = useState("white");

  const [hidden, setHidden] = useState(false);


  const [edditNote, setEditNote] = useState(false);
  
  const[newNote, setNewNote] = useState({
    title: "", 
    content: ""
  });

  const [noteInput, setNoteInput] = useState({
    title: "",
    content: "",
  });

  const pickerStyles = {
    default:{
      picker:{
        // position: "absolute", // neste caso, com estas propriedades fica sempre no mesmo lugar neste caso na nota 1.
        // bottom:"15px",
        // left:"50px",
        marginBottom:"10px",
      }
    }
  };

  const handleDelete = () => {
    props.deleteNote(props.id);
  };

  const handleEdit = () => {
    // setEditNote({
    //   enabled: "true",
    //   title: <input>""</input>,
    //   content: <input>""</input>,
    // });

    setEditNote(true)

    
  };

  const handleChangeTitle = (event) => {
    setNoteInput((prevValue)=>({
      ...prevValue,
      title:event.target.value
    }));
  };
  
  const handleChangeContent = (event) => {
    if(characterLimit - event.target.value.length>=0){
    setNoteInput((prevValue/*vai retornar o object do useState, pois vai selecionar tudo o que tiver em chavetas*/) => ({
      ...prevValue, // isto indica todos os valores do title e content, que neste caso é String e isto serve para substituir o title:prevValue.title e content:prevValue.content
    content:event.target.value
    
    }));
  }};
  // function handleEdit() {
  //   return (

  //   )
  // }

  const handleSave = () => {
    setNewNote({title:noteInput.title, content: noteInput.content});
    setEditNote(false);
  }


  return (
    <div style={{backgroundColor:colorChange}} className='note'>
      <p>{edditNote || newNote.title !== "" ? newNote.title : props.title}</p>
      <p>{edditNote || newNote.content !== "" ? newNote.content : props.content}</p>
      {edditNote ? <div>
        <input onChange={handleChangeTitle} name="title" value={noteInput.title}/>
        <input onChange={handleChangeContent} name="content" value={noteInput.content}/> 
        <small>{characterLimit - noteInput.content.length} Remaining</small>
        <button onClick={handleSave}>SAVE</button>
        </div>: null}
      <button onClick={handleDelete}>DELETE</button>
      <button onClick={handleEdit}>EDIT</button>
      <button onClick={()=> setHidden(!hidden)}><ColorLensIcon/></button>
      <div className="container">
      {hidden && (
      <SketchPicker
      styles={pickerStyles}
      color={colorChange}
      onChange={update => setColorChange(update.hex)}/>
     )}
      </div>
      </div>
  );
}
