import React, { useState } from "react";

import Note from "../Note/Note";
import CreateArea from "../CreateArea/CreateArea";


export default function Container() {
  const [notes, setNotes] = useState([]);


  const addNote = (newNote) => {
    setNotes((prevNotes) => {
      return [...prevNotes, newNote];
    });
  };
  // const editNote = (id) => {
  //   setNotes((prevNotes) => {
  //     return [...prevNotes, id];
  //   });
  // };

  const deleteNote = (id) => {
    setNotes((prevNotes) => {
      return prevNotes.filter((note, index) => { // isto vai retornar um novo array
        return index !== id;  // neste caso, assim que clicas que no botão delete, assim que apagamos a nota que queremos vai gravar esse id e vai comparar a posição(index) dos restantes
      });// só que fica um espaço vazio, por exemplo apagas o 1ª note que tem posição 0, entao fica nota 2 com index 1, a questão é que devia ir para o index 0 e é por isso que se usa o map da linha 32  
    });
  };

  return (
    <div> 
    {/*  isto é um exemplo de um wrapping de se ter no parent component com a ligação a dois ficheiros */}
      <CreateArea addNote={addNote} />
      {notes.map((note, index) => {
        return (
          <Note
            key={index}
            id={index}
            title={note.title}
            content={note.content}
            deleteNote={deleteNote}
            // editNote={editNote}
          />
        );
      })}
    </div>
  );
}
