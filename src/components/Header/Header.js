import React from "react";
import "../../styles/style.css";
import { ThemeProvider } from 'styled-components';
import { lightTheme, darkTheme } from '../Theme/Theme';
import { GlobalStyles } from '../Theme/Global';
import { useDarkMode } from '../Theme/useDarkMode';

export default function Header() {

  const [theme, toggleTheme, componentMounted] = useDarkMode();
  const themeChange = theme === 'light' ? lightTheme : darkTheme;

  if (!componentMounted) {
      return <div />
  };

  return (
    // wrapping 
    <ThemeProvider theme={themeChange}>
    <>
    <GlobalStyles />
    <header>
    <h1>Keeper</h1>
    <button className="btn" onClick={toggleTheme}>Toggle theme</button>
    </header>
    </>
    </ThemeProvider>
  );
}
